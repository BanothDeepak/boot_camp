const data = [{
    23021930923: {
      quantity: 2,
      price: "$3",
      name: "Pen"
    },
    23243243243: {
      quantity: 2,
      price: "$12",
      name: "Book"
    },
    092139029130: {
      quantity: 6,
      price: "$1",
      name: "Eraser"
    },
    839439829489: {
      quantity: 3,
      price: "$6",
      name: "Pencil Box"
    },
    23948234888: {
      quantity: 1,
      price: "$8",
      name: "Geometry Box"
    },
  }]
  
  
  /*
    Q1. use Reduce to calculate totalPrice of all the items 
    [ Total Price = each Item Price * quantity]
  
    Q2. Sort items based on price.
  
    Q3. const money = "$2930.229"
  
        - Split the decimal and integer part of the decimal.
        - Round the number to 2 decimal places
  
    Q4. Write a method to Check and see if an address is a valid IP Address?
      examples =    "0.0.0.0" ,  "343.2.1.4"
  
    Q5. write a single method to convert the following 
       { first: "max", middle: "zeT", last: "Payne" } to "Max Zet Payne"
       { first: "henry", "last": "arnolD"} to "Henry Arnold"
  
    Q6. Write a single method that takes 3 params
        (domain - string, security - boolean, port - number)
        port - default value 80
        security - default value 1
  and do conversion
        (domain - "google.com", security - 1)
         output:      `https://google.com:80`
        
        (domain - "man-utd", security - 0, port - 3000)
        output:       `http://manutd.com:3000`
  
  
  */

        const output = data.reduce((acc,curr)=>{
          for(let item of Object.keys(curr)){
            acc += curr[item].quantity*parseInt(curr[item].price.split("$")[1])
          }
          return acc
        },0)
        
        console.log(output)






const obj = data.reduce((acc,curr)=>{
  for(let i of Object.keys(curr)){
    if(acc[curr[i].quantity]){
      acc[curr[i].quantity] += 1
    }
    else{
      acc[curr[i].quantity] = 1
    }
  }
  return acc
},{})
console.log(obj['2'])













 const priceSort = Object.entries(data[0]).sort((a, b) => {
         return parseInt(a.price) - parseInt(b.price)

        });
 
 console.log(priceSort);

























const money = "$2930.229";
function string1(input){
    let output=[];
    let string="";
    for(let i=0; i< input.length; i++){
        string = input[i].replace("$","");
        if(!(isNaN(string))|| string=="."){
            output.push(string);
        }
    }
    return parseFloat(output.join(''));
}

let intValue = string1(money);
console.log(intValue)

let stringValue = intValue.toString();

const splitValue = stringValue.split(".");

const decimalValue =  splitValue[0];
console.log("Decimal Part : " + decimalValue);

const integerValue =  splitValue[1];
console.log("Integer Part : " + integerValue);

const roundValue = Math.round(intValue * 100)/100;
console.log("Value after round up to 2 decimal places : $"+roundValue)//3












function solution4(input){
  let array=input.split('.');

  let finalIntegerAddress=[];
  
  for(let index=0;index<array.length;index++){
      let number=parseInt(array[index]);
  
      if((0<=number && number<=255)){
          if(!(Number.isNaN(number))){
              finalIntegerAddress.push(number);
          }
      }
  }
  
  if(finalIntegerAddress.length===4){
     console.log("Valid IP Address");
  } else{
     console.log("Invalid IP Address");
  }
}
solution4("0.0.0.0");
solution4("343.2.1.4");//4





function string4(inputName){
  let tempString='';
  let { first_name: firstName, middle_name: middleName, last_name: lastName } = inputName;
  if(!firstName || !lastName) {
      return '';
  } else if(middleName) {
      tempString=`${firstName} ${middleName} ${lastName}`;
  } else {
      tempString=`${firstName} ${lastName}`;
  }
  let outputString=tempString.split(' ');   
  
  for(let word in outputString){
      outputString[word]=outputString[word].toLowerCase().split('');
      
      outputString[word][0]=outputString[word][0].toUpperCase();
      
      outputString[word]=outputString[word].join('');
  }
  return outputString.join(' '); 
}
let inputName1 = {"first_name": "max", "middle_name":"zeT",  "last_name": "Payne"};
console.log(string4(inputName1));

let inputName2 = {"first_name": "henry", "last_name": "arnolD"};
console.log(string4(inputName2));//5








function solution6(domain = " ", security = 1, port = 80){
  if(!domain){
      console.log("Enter a domain name");
  } else {
      if(security === 1 ){
          console.log(`https://${domain}:${port}`);
      } else {
          console.log(`http://${domain}:${port}`)
      }
  }
}
solution6("man-utd.com", 0, 3000);
solution6("google.com", 1);
