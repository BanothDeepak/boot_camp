const input = [{
    "userId": 1,
    "id": 1,
    "title": "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
    "body": "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto"
  },
  {
    "userId": 1,
    "id": 2,
    "title": "qui est esse",
    "body": "est rerum tempore vitae\nsequi sint nihil reprehenderit dolor beatae ea dolores neque\nfugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis\nqui aperiam non debitis possimus qui neque nisi nulla"
  },
  {
    "userId": 1,
    "id": 3,
    "title": "ea molestias quasi exercitationem repellat qui ipsa sit aut",
    "body": "et iusto sed quo iure\nvoluptatem occaecati omnis eligendi aut ad\nvoluptatem doloribus vel accusantium quis pariatur\nmolestiae porro eius odio et labore et velit aut"
  },
  {
    "userId": 1,
    "id": 4,
    "title": "eum et est occaecati",
    "body": "ullam et saepe reiciendis voluptatem adipisci\nsit amet autem assumenda provident rerum culpa\nquis hic commodi nesciunt rem tenetur doloremque ipsam iure\nquis sunt voluptatem rerum illo velit"
  },
  {
    "userId": 1,
    "id": 5,
    "title": "nesciunt quas odio",
    "body": "repudiandae veniam quaerat sunt sed\nalias aut fugiat sit autem sed est\nvoluptatem omnis possimus esse voluptatibus quis\nest aut tenetur dolor neque"
  },
  {
    "userId": 3,
    "id": 6,
    "title": "dolorem eum magni eos aperiam quia",
    "body": "ut aspernatur corporis harum nihil quis provident sequi\nmollitia nobis aliquid molestiae\nperspiciatis et ea nemo ab reprehenderit accusantium quas\nvoluptate dolores velit et doloremque molestiae"
  },
  {
    "userId": 1,
    "id": 7,
    "title": "magnam facilis autem",
    "body": "dolore placeat quibusdam ea quo vitae\nmagni quis enim qui quis quo nemo aut saepe\nquidem repellat excepturi ut quia\nsunt ut sequi eos ea sed quas"
  },
  {
    "userId": 1,
    "id": 8,
    "title": "dolorem dolore est ipsam",
    "body": "dignissimos aperiam dolorem qui eum\nfacilis quibusdam animi sint suscipit qui sint possimus cum\nquaerat magni maiores excepturi\nipsam ut commodi dolor voluptatum modi aut vitae"
  },
  {
    "userId": 1,
    "id": 9,
    "title": "nesciunt iure omnis dolorem tempora et accusantium",
    "body": "consectetur animi nesciunt iure dolore\nenim quia ad\nveniam autem ut quam aut nobis\net est aut quod aut provident voluptas autem voluptas"
  },
  {
    "userId": 3,
    "id": 10,
    "title": "optio molestias id quia eum",
    "body": "quo et expedita modi cum officia vel magni\ndoloribus qui repudiandae\nvero nisi sit\nquos veniam quod sed accusamus veritatis error"
  }]

  
  /*


    Q1. Print the last object of the data array without using a loop.

    Q2. Print the object having id = 9.

    Q3. Form two arrays.
        First Array should have first 5 objects. 
        Second Array should have the rest others.

    Q4. Form two arrays 
        First Array should contain all objects having user id 1
        Second Array should contain all objects having user id 3

    Q5. Sort array (Ascending Order)
        A. Based on length of body "key".
        B. Based on userIds .
        C. Based on title .
        D. Based on ids (Descending Order).
  */
##ANSWERS:

1) console.log(input[input.length -1])

2) console.log(input[input.length -2])

3) const size = 5;
   const firstArray = input.slice(0, size)
   console.log(firstArray);
   const secondArray = input.slice(size);
   console.log(secondArray);


4th answer:

let first =[];
let second =[];

for (let i = 0; i < input.length; i++) {
    if (input[i]["userId"] == '1') {
        console.log("userId");
        first.push(input[i])
    }else {
        console.log("Different");
        second.push(input[i])
    }
}
console.log(first)
console.log(second)





5th answer:


input.sort((a, b ) => {
    return a.userId - b.userId;
});
console.log(input);



input.sort((a, b ) => {
    return a.title.length - b.title.length;
});


console.log(input);
input.sort((a, b ) => {
    return a.body.length - b.body.length;
});
console.log(input);


input.sort((a,b) => {
    if( a.id.length < b.id.length){
        return  1;
    } else {
        return -1;
    }
    
});
console.log(input);