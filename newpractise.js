const data = {
    person_info_1: {
        profile: {
            fullName: "Javier Hernandez",
            nationality: {
                country: "Mexico",
            },
            tel: 904902394
        },
        current_address: {
            current_city: {
                value: "Bangalore",
                zip: '399993'
            }
        }
    },

    person_info_2: {
        profile: {
            fullName: "Emily Spade",
            nationality: {
                country: "Norway",
            },
            tel: 309320239
        },
        current_address: {
            current_city: {
                value: "Oslo",
                zip: '239292'
            }
        }
    },
    person_info_3: {
        profile: {
            fullName: "John Cigan",
            nationality: {
                country: "Turkey",
            },
            tel: 932483988
        },
        current_address: {
            current_city: {
                value: "Istanbul",
                zip: '932099'
            }
        }
    },
    person_info_4: {
        profile: {
            fullName: "Marsh Hobbs",
            nationality: {
                country: "USA",
            },
            tel: 32043988
        },
        current_address: {
            current_city: {
                value: "Istanbul",
                zip: '932099'
            }
        }
    }


}

let arr = Object.entries(data).filter((person) => person[1].current_address.current_city.value === "Istanbul");
console.log(arr);


/*
    Q1. Get all persons whose current city  is Istanbul .
        A. Get result in objects form 
        {
            person_info: {
                // that person info
            }
        }

        B. Get result in array Form
        [{// Info of the person }, { // Info of the person}]

        use Both filter and reduce.

    Q2. Copy data object properly without leaving any reference.

    Q3. Use map to get me all nationality and current location for each person.
        use person's name as key to store the person's nationality and current_address.*/



  const obj = [];
  const result = Object.entries(data).filter((element) => {
    return (element[1].current_address.current_city.value === "Istanbul")
    // obj.push(element[i])
  })
  console.log(result)//1

 const res = Object.keys(data).reduce((acc, cur) => {
    if((data[cur].current_address.current_city.value)==='Istanbul'){
        acc +=1
    }
    return acc
},0);
 
 console.log(res)//1.2

const res = Object.entries(data).map((element) => element[1].profile.nationality.country)

console.log(res)//3

































// let element = Object.entries(data);
//  let dataValue = [];
//  element.forEach((obj)=>{
//     dataValue.push(obj[1]);
//  })
// const cities = dataValue.filter((element)=> (element.current_address.current_city.value == 'Istanbul'));
// const person = cities.map((city)=> city.profile);
// // Get result of array form
// console.log(person,'person in array form');
// // Get reult of object form
// person.forEach((personInfo)=>{
//     console.log(personInfo, 'person in object form');
// })
// const personInfo = dataValue.map(person => ({ nationality: person.profile.nationality, current_address: person.current_address }))
// console.log(personInfo, "person's nationality and current address");



//  let copiedData = data;
//  console.log(copiedData,'copiedData');

